import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Signo } from './../../../_model/signo';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { SignoService } from './../../../_service/signo.service';
import { PacienteService } from './../../../_service/paciente.service';
import { Paciente } from './../../../_model/paciente';
import { Observable } from 'rxjs';
import { MatSnackBar } from '@angular/material';
import { map } from 'rxjs/operators';


@Component({
  selector: 'app-signo-edicion',
  templateUrl: './signo-edicion.component.html',
  styleUrls: ['./signo-edicion.component.css']
})
export class SignoEdicionComponent implements OnInit {
  [x: string]: any;

  id: number;
  form: FormGroup;
  edicion: boolean = false;
  signo: Signo;

 //form: FormGroup;
  //signoSeleccionado: Signo;

  pacientes: Paciente[] = [];
  myControlPaciente: FormControl = new FormControl();

  mensaje: string;

  filteredOptions: Observable<any[]>;
  pacienteSeleccionado: Paciente;

  fechaSeleccionada: Date = new Date();
  maxFecha: Date = new Date();

  // tslint:disable-next-line:max-line-length
  constructor(private builder: FormBuilder, private route: ActivatedRoute, private router: Router, private pacienteService: PacienteService, private signoService: SignoService, public snackBar: MatSnackBar) { }

  ngOnInit() {
/*
    this.form = new FormGroup({
      'id': new FormControl(0),
      'fecha': new FormControl(''),
      'temperatura': new FormControl(''),
      'pulso': new FormControl(''),
      'ritmo': new FormControl(''),
      'paciente': this.myControlPaciente
      //'idpaciente': new FormControl(''),
    });
    */

   this.form = this.builder.group({
    'id': new FormControl(0),
    'fecha': new FormControl(new Date()),
    'temperatura': new FormControl(''),
    'pulso': new FormControl(''),
    'ritmo': new FormControl(''),
    'paciente': this.myControlPaciente
  });
    this.listarPacientes();
    this.filteredOptions = this.myControlPaciente.valueChanges.pipe(map(val => this.filter(val)));

    this.signo = new Signo();

    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = this.id != null;

      this.initForm();
    });
  }

  initForm() {
    if (this.edicion) {
      //cargar la data del servicio hacia el form  
      this.signoService.listarSignoPorId(this.id).subscribe(data => {
        this.form = new FormGroup({
          'id': new FormControl(data.idSigno),
          'fecha': new FormControl((data.fecha).toString()),
          'temperatura': new FormControl(data.temperatura),
          'pulso': new FormControl(data.pulso),
          'ritmo': new FormControl(data.ritmo),
          'paciente': new FormControl(data.paciente)
      });
      });
    }
  }

  operar() {
    this.signo.idSigno = this.form.value['id'];
    var tzoffset = (this.form.value['fecha']).getTimezoneOffset() * 60000;
    var localISOTime = (new Date(Date.now() - tzoffset)).toISOString()
    this.signo.fecha = localISOTime;
    //this.signo.fecha = this.form.value['fecha'];
    this.signo.temperatura = this.form.value['temperatura'];
    this.signo.pulso = this.form.value['pulso'];
    this.signo.ritmo = this.form.value['ritmo'];
    this.signo.paciente = this.form.value['paciente'];
    
    if (this.edicion) {
      //actualizar
      this.signoService.modificar(this.signo).subscribe(data => {
        this.signoService.listar().subscribe(signos => {
          this.signoService.signoCambio.next(signos);
          this.signoService.mensajeCambio.next('Se modificó');
        });
      });
    } else {
      //registrar
      this.signoService.registrar(this.signo).subscribe(data => {
        this.signoService.listar().subscribe(signos => {
          this.signoService.signoCambio.next(signos);
          this.signoService.mensajeCambio.next('Se registró');
        });
      });
    }

    this.router.navigate(['signo']);
  }

  filter(val: any) {
    if (val != null && val.idPaciente > 0) {
      return this.pacientes.filter(option =>
        option.nombres.toLowerCase().includes(val.nombres.toLowerCase()) || option.apellidos.toLowerCase().includes(val.apellidos.toLowerCase()) || option.dni.includes(val.dni));
    } else {
      return this.pacientes.filter(option =>
        option.nombres.toLowerCase().includes(val.toLowerCase()) || option.apellidos.toLowerCase().includes(val.toLowerCase()) || option.dni.includes(val));
    }
  }
  displayFn(val: Paciente) {
    return val ? `${val.nombres} ${val.apellidos}` : val;
  }

  listarPacientes() {
    this.pacienteService.listar().subscribe(data => {
      this.pacientes = data;
    });
  }

  seleccionarPaciente(e: any){
    //console.log(e);
    this.pacienteSeleccionado = e.option.value;
  }

  

}
