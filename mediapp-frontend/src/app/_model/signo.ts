import { Paciente } from "./paciente";

export class Signo {
    public idSigno: number;
    public fecha: string;
    public temperatura: string;
    public pulso: string;
    public ritmo: string;
    public paciente: Paciente;
    public email: string;
}